package edu.tus.microservices.wine.dao;

public enum Rating
{
    CLASSIC,
    OUTSTANDING,
    VERY_GOOD,
    GOOD,
    MEDIOCRE,
    NOT_RECOMMENDED;
}
