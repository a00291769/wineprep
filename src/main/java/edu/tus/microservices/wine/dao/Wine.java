package edu.tus.microservices.wine.dao;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.persistence.EnumType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;
import java.time.Year;



@Transactional
@Entity
@Table(name="wine")
public class Wine
{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Country cannot be blank.")
    private String country;

    @Column(name = "expiry_date")
    private java.time.LocalDate expiryDate;

    @NotBlank(message = "Grapes cannot be blank.")
    private String grapes;

    @NotBlank(message = "Name cannot be blank.")
    private String name;

    @Enumerated(EnumType.STRING)
    private Rating rating;

    @NotBlank(message = "Region cannot be blank.")
    private String region;

    @Min(1500)
    @Max(2022)
    private int year;

    public Wine()
    {

    }

    public Wine(Long id, String country, LocalDate expiryDate, String grapes, Rating rating, String region, int year)
    {
        this.id = id;
        this.country = country;
        this.expiryDate = expiryDate;
        this.grapes = grapes;
        this.rating = rating;
        this.region = region;
        this.year = year;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public LocalDate getExpiryDate()
    {
        return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate)
    {
        this.expiryDate = expiryDate;
    }

    public String getGrapes()
    {
        return grapes;
    }

    public void setGrapes(String grapes)
    {
        this.grapes = grapes;
    }

    public Rating getRating()
    {
        return rating;
    }

    public void setRating(Rating rating)
    {
        this.rating = rating;
    }

    public String getRegion()
    {
        return region;
    }

    public void setRegion(String region)
    {
        this.region = region;
    }

    public int getYear()
    {
        return year;
    }

    public void setYear(int year)
    {
        this.year = year;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
