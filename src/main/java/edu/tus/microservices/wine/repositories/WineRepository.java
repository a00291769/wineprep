package edu.tus.microservices.wine.repositories;

import edu.tus.microservices.wine.dao.Wine;

import edu.tus.microservices.wine.dao.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface WineRepository extends JpaRepository<Wine, Long>
{
    public List<Optional<Wine>> findByRating(Rating rating);

    public List<Optional<Wine>> findByName(String name);

    public List<Optional<Wine>> findByCountry(String country);

    public List<Optional<Wine>> findByCountryAndRating(String country, Rating rating);

    public List<Optional<Wine>> findAllByRating(Rating rating, Pageable page);

    public List<Optional<Wine>> findFirstByExpiryDate(LocalDate date);
}
