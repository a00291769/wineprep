package edu.tus.microservices.wine.controllers.v1;

import edu.tus.microservices.wine.dao.Wine;
import edu.tus.microservices.wine.dao.Rating;
import edu.tus.microservices.wine.exceptions.WineNotFoundException;
import edu.tus.microservices.wine.repositories.WineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RestController
@Service
@RequestMapping("wine/api/v1")
public class WineService
{
    @Autowired
    WineRepository wineRepo;

    // GET - Get all the wines
    @GetMapping("/wines")
    List<Wine> getAllWines()
    {
        System.out.println("Get all wines");

        List<Wine> wines = wineRepo.findAll();

        if(wines.isEmpty())
        {
            throw new WineNotFoundException("No Wines Found");
        }
        else
        {
            return wineRepo.findAll();
        }
    }


//    // GET - Get all the wines - sorted by Rating and with offset and limit, no pagination
//    @GetMapping("/wines")
//    List<Optional<Wine>> getAllWines(@RequestParam(defaultValue = "0") int offset, @RequestParam(defaultValue = "2") int limit, @RequestParam(defaultValue = "CLASSIC") Rating rating)
//    {
//        System.out.println("Get all wines");
//
//        Pageable firstPageWithFourElements = PageRequest.of(offset, limit, Sort.by("country"));
//
//        List<Optional<Wine>> wines = wineRepo.findAllByRating(rating, firstPageWithFourElements);
//
//        return wines;
//    }


//    // GET - Get all the wines with an offset and limit
//    @GetMapping("/wines")
//    Page<Wine> getAllWines(@RequestParam(defaultValue = "0") int offset, @RequestParam(defaultValue = "2") int limit,
//            @RequestParam(defaultValue = "id") String sortkey)
//    {
//        System.out.println("Get all wines");
//        System.out.println(sortkey);
//
//        Pageable selection = PageRequest.of(offset, limit, Sort.by(sortkey));
//
//        Page<Wine> wines = wineRepo.findAll(selection);
//
//        return wines;
//    }

    // GET - Get a wine by ID
    @RequestMapping("/wines/{ID}")
    Optional<Wine> getWineByID(@PathVariable("ID") long ID)
    {
        System.out.println("Get wine by ID");

        Optional<Wine> wine = wineRepo.findById(ID);

        if(wine.isPresent())
        {
            return wine;
        }
        else
        {
            throw new WineNotFoundException("No Wine with ID: " + ID);
        }
    }


    @GetMapping("/pagination") //printing 4 elements
    public Page<Wine> page()
    {
        PageRequest firstPageWithFourElements = PageRequest.of(0, 4,Sort.by("expiryDate").and(Sort.by("country")));
        Page<Wine> page = wineRepo.findAll(firstPageWithFourElements);

        return page;
    }

    @GetMapping("/pageable")
    Page<Wine> getTasksByPage(Pageable pageable)
    {
        Page<Wine> taskPage=wineRepo.findAll(pageable);
        return taskPage;
    }

    @GetMapping("/wines/expiry")
    List<Optional<Wine>> getByDate(@RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                           LocalDate date)
    {
        System.out.println(date);
        System.out.println("Get by Date");
        List<Optional<Wine>> taskPage=wineRepo.findFirstByExpiryDate(date);
        return taskPage;
    }

    // GET - Get a wine by Rating
    @RequestMapping("/wines/ratings")
    List<Optional<Wine>> getWineByRating(@RequestParam(defaultValue = "0") int offset, @RequestParam(defaultValue = "2") int limit, @RequestParam(defaultValue = "CLASSIC") Rating rating,
                                         @RequestParam(defaultValue = "id") String sortkey)
    {
        System.out.println("Get wine by rating");
        System.out.println(rating);

        // Sort is optional
        Pageable firstPageWithFourElements = PageRequest.of(offset, limit, Sort.by(sortkey));
        List<Optional<Wine>> wines = wineRepo.findAllByRating(rating, firstPageWithFourElements);

//        List<Optional<Wine>> wines = wineRepo.findByRating(rating);

        if(wines.isEmpty())
        {
            throw new WineNotFoundException("No Wine with Rating: " + rating);
        }
        else
        {
            return wines;
        }
    }

    // GET - Get a wine by Country
    @RequestMapping("/wines/countries")
    List<Optional<Wine>> getWineByCountry(@RequestParam("country") String country)
    {
        System.out.println("Get wine by country");
        System.out.println(country);

        List<Optional<Wine>> wines = wineRepo.findByCountry(country);

        if(wines.isEmpty())
        {
            throw new WineNotFoundException("No Wine from Country: " + country);
        }
        else
        {
            return wines;
        }
    }

    // GET - Get a wine by name
    @RequestMapping("/wines/names")
    List<Optional<Wine>> getWineByName(@RequestParam("name") String name)
    {
        System.out.println("Get wine by name");
        System.out.println(name);

        List<Optional<Wine>> wines = wineRepo.findByName(name);

        if(wines.isEmpty())
        {
            throw new WineNotFoundException("No Wine from Label: " + name);
        }
        else
        {
            return wines;
        }
    }

    // GET - Get a wine by Country and Rating
    @RequestMapping("/wines/countries/ratings")
    List<Optional<Wine>> getWineByCountryAndRating(@RequestParam("country") String country, @RequestParam Rating rating)
    {
        System.out.println("Get wine by country and rating");
        System.out.println(country);
        System.out.println(rating);

        List<Optional<Wine>> wines = wineRepo.findByCountryAndRating(country, rating);

        if(wines.isEmpty())
        {
            throw new WineNotFoundException("No Wine from Country: " + country + " with Rating: " + rating);
        }
        else
        {
            return wines;
        }
    }

    // POST -  Insert a new wine
    @RequestMapping(value = "/wines", method = RequestMethod.POST)
    ResponseEntity insertWine(@Valid @RequestBody Wine wine)
    {
        System.out.println("Insert a new wine");

        Wine savedWine = wineRepo.save(wine);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{ID}")
                .buildAndExpand((savedWine.getId()))
                .toUri();

        return ResponseEntity.created(location).build();
    }

    // PUT - Update a player by ID
    @RequestMapping(value = "/wines/{ID}", method = RequestMethod.PUT)
    ResponseEntity<Wine> updateWineByID(@Valid @PathVariable long ID, @RequestBody Wine wine)
    {
        System.out.println("Update a player by ID");

        // Get the current value of Player
        Optional<Wine> optionalWine = wineRepo.findById(ID);

        // Validate wine exists before trying to update it
        if(optionalWine.isPresent())
        {
            Wine existingWine = optionalWine.get();
            // Update each field
            existingWine.setName(wine.getName());
            existingWine.setCountry(wine.getCountry());
            existingWine.setExpiryDate(wine.getExpiryDate());
            existingWine.setGrapes(wine.getGrapes());
            existingWine.setRating(wine.getRating());
            existingWine.setRegion(wine.getRegion());
            existingWine.setYear(wine.getYear());

            // Save all the new fields
            Wine savedWine = wineRepo.save(existingWine);

            // Return the Player details as response
            return new ResponseEntity<Wine>(savedWine, HttpStatus.ACCEPTED);
        }
        else
        {
            throw new WineNotFoundException("No Wine with ID: " + ID);
        }
    }

    // DELETE - Delete a wine by ID
    @RequestMapping(value = "/wines/{ID}", method = RequestMethod.DELETE)
    // Return an empty response body with status code 204 to indicate success
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    void deleteWineByID(@PathVariable long ID)
    {
        System.out.println("Delete by ID");

        Optional<Wine> optionalWine = wineRepo.findById(ID);

        // Validate a wine exists before trying to delete it
        if(optionalWine.isPresent())
        {
            Wine existingWine = optionalWine.get();

            wineRepo.delete(existingWine);
        }
        else
        {
            throw new WineNotFoundException("No Wine with ID: " + ID);
        }
    }
}
